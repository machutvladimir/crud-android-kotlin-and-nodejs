const express = require('express');
const sqlite3 = require('sqlite3').verbose();

const app = express();
const port = 3000;

// Create a SQLite database connection
const db = new sqlite3.Database('sqlite.db');

// Create the table if it doesn't exist
db.run(`
  CREATE TABLE IF NOT EXISTS pret (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    num_compte INTEGER,
    nom_client TEXT,
    nom_banque TEXT,
    montant REAL,
    taux REAL,
    date_pret datetime default current_timestamp
  )
`);

// Middleware to parse JSON requests
app.use(express.json());

// API endpoint to retrieve all data from the database
app.get('/api/pret', (req, res) => {
  db.all('SELECT * FROM pret', (err, rows) => {
    if (err) throw err;
    res.json(rows);
  });
});

// API endpoint to retrieve a specific record
app.get('/api/pret/:id', (req, res) => {
  db.get('SELECT * FROM pret WHERE id = ?', [req.params.id], (err, row) => {
    if (err) throw err;
    if (!row) {
      return res.status(404).json({ error: 'Record not found' });
    }
    res.json(row);
  });
});

// API endpoint to create a new record
app.post('/api/pret', (req, res) => {
  const { num_compte, nom_client, nom_banque, montant, taux } = req.query;
  db.run(
    'INSERT INTO pret (num_compte, nom_client, nom_banque, montant, taux) VALUES (?, ?, ?, ?, ?)',
    [num_compte, nom_client, nom_banque, montant, taux],
    function (err) {
      if (err) throw err;
      db.get('SELECT * FROM pret WHERE id = ?', [this.lastID], (err, row) => {
        if (err) throw err;
        res.status(201).json(row);
      });
    }
  );
});

// API endpoint to update a record
app.put('/api/pret/:id', (req, res) => {
  const { num_compte, nom_client, nom_banque, montant, taux, date_pret } = req.query;
  db.run(
    'UPDATE pret SET num_compte = ?, nom_client = ?, nom_banque = ?, montant = ?, taux = ?, date_pret = ? WHERE id = ?',
    [num_compte, nom_client, nom_banque, montant, taux, date_pret, req.params.id],
    (err) => {
      if (err) throw err;
      db.get('SELECT * FROM pret WHERE id = ?', [req.params.id], (err, row) => {
        if (err) throw err;
        res.json(row);
      });
    }
  );
});

// API endpoint to delete a record
app.delete('/api/pret/:id', (req, res) => {
  db.run('DELETE FROM pret WHERE id = ?', [req.params.id], (err) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    res.json({ message: 'Record deleted' });
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});