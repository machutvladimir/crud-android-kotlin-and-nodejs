package com.machut.pretmanagement

import android.os.AsyncTask
import com.machut.pretmanagement.PretRepository.Singleton.maxAmount
import com.machut.pretmanagement.PretRepository.Singleton.minAmount
import com.machut.pretmanagement.PretRepository.Singleton.pretList
import com.machut.pretmanagement.PretRepository.Singleton.totalAmount
import okhttp3.Call
import okhttp3.Callback
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class PretRepository {
    object Singleton {
        var pretList = arrayListOf<PretModel>()
        var totalAmount :Float = 0f
        var minAmount :Float = 0f
        var maxAmount :Float = 0f

        init {
            val repository = PretRepository()
            repository.fetchData()
        }
    }
    fun fetchData(){
        pretList.clear()

        val url = "http://10.42.0.1:3000/api/pret"
        val request = Request.Builder()
            .url(url)
            .build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                // Handle the failure case
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    // Parse the response and update the pretList
                    val jsonArray = JSONArray(response.body?.string())
                    for (i in 0 until jsonArray.length()) {
                        val jsonObject = jsonArray.getJSONObject(i)
                        val pret = PretModel(
                            jsonObject.getString("id"),
                            jsonObject.getString("num_compte"),
                            jsonObject.getString("nom_client"),
                            jsonObject.getString("nom_banque"),
                            jsonObject.getDouble("montant").toFloat(),
                            jsonObject.getString("date_pret"),
                            jsonObject.getDouble("taux").toFloat()
                        )
                        pretList.add(pret)
                        if(totalAmount != 0f)totalAmount += pret.montant else totalAmount = pret.montant
                        minAmount = if(minAmount != 0f) minAmount.coerceAtMost(pret.montant) else pret.montant
                        maxAmount = if(maxAmount != 0f) maxAmount.coerceAtLeast(pret.montant) else pret.montant
                    }
                } else {
                    // Handle the error case
                    response.code
                }
            }
        })
    }
    fun update(pretToUpdate: PretModel, recyclerAdapter: RecyclerAdapter) {
        val url = "http://10.42.0.1:3000/api/pret/${pretToUpdate.id}"

        val urlBuilder = url.toHttpUrlOrNull()!!.newBuilder()
            .addQueryParameter("num_compte", pretToUpdate.num)
            .addQueryParameter("nom_client", pretToUpdate.nom)
            .addQueryParameter("nom_banque", pretToUpdate.banque)
            .addQueryParameter("montant", pretToUpdate.montant.toString())
            .addQueryParameter("date_pret", pretToUpdate.datePicker)
            .addQueryParameter("taux", pretToUpdate.taux.toString())
            .build()

        val request = Request.Builder()
            .url(urlBuilder)
            .put(RequestBody.create(null, ""))
            .build()

        object : AsyncTask<Void, Void, Boolean>() {
            override fun doInBackground(vararg params: Void?): Boolean {
                val client = OkHttpClient()
                val response = client.newCall(request).execute()
                return response.isSuccessful
            }

            override fun onPostExecute(result: Boolean) {
                super.onPostExecute(result)
                if (result) {
                    val updatedPretIndex = pretList.indexOfFirst { it.id == pretToUpdate.id }
                    if (updatedPretIndex != -1) {
                        pretList[updatedPretIndex] = pretToUpdate // Update the local data model
                        updateTotals(pretToUpdate, isAdd = false)
                        recyclerAdapter.notifyItemChanged(updatedPretIndex) // Notify the adapter
                    }
                }
            }
        }.execute()
    }

    fun delete(pretToDelete: PretModel, positionId: Int, recyclerAdapter: RecyclerAdapter) {
        val url = "http://10.42.0.1:3000/api/pret/${pretToDelete.id}"

        val request = Request.Builder()
            .url(url)
            .delete()
            .build()

        object : AsyncTask<Void, Void, Boolean>() {
            override fun doInBackground(vararg params: Void?): Boolean {
                val client = OkHttpClient()
                val response = client.newCall(request).execute()
                return response.isSuccessful
            }

            override fun onPostExecute(result: Boolean) {
                super.onPostExecute(result)
                if (result) {
                    val deletedItemIndex = pretList.indexOfFirst { it.id == pretToDelete.id }
                    if (deletedItemIndex != -1) {
                        updateTotals(pretToDelete, isAdd = false)
                        pretList.removeAt(deletedItemIndex) // Remove the deleted item from the local data model
                        recyclerAdapter.notifyItemRemoved(deletedItemIndex) // Notify the adapter
                    }
                }
            }
        }.execute()
    }

    fun add(pretToAdd: PretModel, recyclerAdapter: RecyclerAdapter) {
        val url = "http://10.42.0.1:3000/api/pret"

        val urlBuilder = url.toHttpUrlOrNull()!!.newBuilder()
            .addQueryParameter("num_compte", pretToAdd.num)
            .addQueryParameter("nom_client", pretToAdd.nom)
            .addQueryParameter("nom_banque", pretToAdd.banque)
            .addQueryParameter("montant", pretToAdd.montant.toString())
            .addQueryParameter("taux", pretToAdd.taux.toString())
            .build()

        val request = Request.Builder()
            .url(urlBuilder)
            .post(RequestBody.create(null, ""))
            .build()

        object : AsyncTask<Void, Void, Boolean>() {
            override fun doInBackground(vararg params: Void?): Boolean {
                val client = OkHttpClient()
                val response = client.newCall(request).execute()
                return response.isSuccessful
            }

            override fun onPostExecute(result: Boolean) {
                super.onPostExecute(result)
                if (result) {
                    pretList.add(pretToAdd) // Add the new item to the local data model
                    updateTotals(pretToAdd, isAdd = true)
                    recyclerAdapter.notifyItemInserted(pretList.size - 1) // Notify the adapter
                } else {
                    // Handle add failure (e.g., show error message)
                }
            }
        }.execute()
    }

    private fun updateTotals(pret: PretModel, isAdd: Boolean) {
        if (isAdd) {
            totalAmount += pret.montant
            minAmount = minOf(minAmount, pret.montant)
            maxAmount = maxOf(maxAmount, pret.montant)
        } else {
            totalAmount -= pret.montant
            minAmount = if (pretList.isNotEmpty()) pretList.minByOrNull { it.montant }?.montant ?: 0f else 0f
            maxAmount = if (pretList.isNotEmpty()) pretList.maxByOrNull { it.montant }?.montant ?: 0f else 0f
        }
    }
}