package com.machut.pretmanagement

import android.app.ActionBar
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.machut.pretmanagement.PretRepository.Singleton.maxAmount
import com.machut.pretmanagement.PretRepository.Singleton.minAmount
import com.machut.pretmanagement.PretRepository.Singleton.pretList
import com.machut.pretmanagement.PretRepository.Singleton.totalAmount
import java.util.Calendar


class MainActivity : AppCompatActivity() {
    val context: MainActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val total = findViewById<TextView>(R.id.totalValue)
        val min = findViewById<TextView>(R.id.minimumValue)
        val max = findViewById<TextView>(R.id.maximumValue)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerAdapter(this, pretList,total, min, max,R.layout.recycler_view_item)

        System.err.println(pretList.size)

        showAddPopup()

        val addBtn = findViewById<FloatingActionButton>(R.id.add_btn)
        addBtn.setOnClickListener {
            Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show()
            PopupAdd(this, RecyclerAdapter(this, pretList,total, min, max,R.layout.recycler_view_item)).show()
        }
    }


    private fun  showAddPopup(){
        val layoutInflater = LayoutInflater.from(this)
        val popupView = layoutInflater.inflate(R.layout.popup_add, null)

        val popupWindow = PopupWindow(
            popupView,
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            false
        )

        val addBtn = findViewById<FloatingActionButton>(R.id.add_btn)
        addBtn.setOnClickListener {
            Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show()
            popupWindow.showAtLocation(findViewById(R.id.add_btn), Gravity.CENTER, 0, 0)

        }
    }
}