package com.machut.pretmanagement

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.machut.pretmanagement.PretRepository.Singleton.maxAmount
import com.machut.pretmanagement.PretRepository.Singleton.minAmount
import com.machut.pretmanagement.PretRepository.Singleton.totalAmount
import kotlin.math.roundToLong

class RecyclerAdapter(
    val context: MainActivity,
    private val pretList: List<PretModel>,
    private var totalValue: TextView,
    private var minValue: TextView,
    private var maxValue: TextView,
    private val layoutId: Int
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val itemId: TextView = itemView.findViewById(R.id.idItem)
        val itemNom: TextView = itemView.findViewById(R.id.nomItem)
        val itemBanque: TextView = itemView.findViewById(R.id.banqueItem)
        val itemMontant: TextView = itemView.findViewById(R.id.montantItem)
        val itemDate: TextView = itemView.findViewById(R.id.dateItem)
        val itemAPayer: TextView = itemView.findViewById(R.id.apayerItem)

        init {
            itemView.setOnClickListener { v: View ->
                val position: Int = adapterPosition
                Toast.makeText(itemView.context, "You clicked on item #${position + 1}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        totalValue.text = totalAmount.toString()
        minValue.text = minAmount.toString()
        maxValue.text = maxAmount.toString()

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return pretList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentPret = pretList[position]

        // Recuperer le repo...

        holder.itemId.text = String.format("%d", position + 1)
        holder.itemNom.text = currentPret.nom
        holder.itemBanque.text = currentPret.banque
        holder.itemMontant.text = currentPret.montant.toString()
        holder.itemDate.text = currentPret.datePicker
        holder.itemAPayer.text = String.format("%.2f", (currentPret.montant) * (1 + currentPret.taux / 100))

        holder.itemView.setOnClickListener {
            PopupEdit(this, currentPret, position).show()
        }
    }
}