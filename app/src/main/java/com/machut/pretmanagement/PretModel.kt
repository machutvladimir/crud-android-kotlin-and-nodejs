package com.machut.pretmanagement

class PretModel(
    val id: String,
    val num: String,
    val nom: String,
    val banque: String,
    val montant: Float,
    val datePicker: String,
    val taux: Float
) {
}