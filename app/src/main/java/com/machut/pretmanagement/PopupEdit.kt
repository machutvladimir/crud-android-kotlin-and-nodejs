package com.machut.pretmanagement

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView

class PopupEdit(
    private val adapter: RecyclerAdapter,
    private val currentPret: PretModel,
    private val position: Int
) : Dialog(adapter.context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.popup_edit)

        findViewById<EditText>(R.id.editTextEdit).setText(currentPret.num)
        findViewById<EditText>(R.id.editTextEdit2).setText(currentPret.nom)
        findViewById<EditText>(R.id.editTextEdit3).setText(currentPret.banque)
        findViewById<EditText>(R.id.editTextEdit4).setText(currentPret.montant.toString())
        findViewById<EditText>(R.id.editTextEdit5).setText(currentPret.taux.toString())


        setupEditButton()
        setupDeleteButton()
        setupCloseButton()
    }

    private fun setupEditButton() {
        findViewById<ImageView>(R.id.imageView2).setOnClickListener {
                v: View ->
            val modifiedPret = PretModel(
                currentPret.id,
                findViewById<EditText>(R.id.editTextEdit).text.toString(),
                findViewById<EditText>(R.id.editTextEdit2).text.toString(),
                findViewById<EditText>(R.id.editTextEdit3).text.toString(),
                findViewById<EditText>(R.id.editTextEdit4).text.toString().toFloat(),
                currentPret.datePicker,
                findViewById<EditText>(R.id.editTextEdit5).text.toString().toFloat()
            )
            PretRepository().update(modifiedPret, adapter)

            dismiss()
        }
    }

    private fun setupDeleteButton() {
        findViewById<ImageView>(R.id.imageView).setOnClickListener {
            v: View ->
            PretRepository().delete(currentPret, position,adapter)

            dismiss()
        }
    }

    private fun setupCloseButton() {
        findViewById<ImageView>(R.id.icon_close).setOnClickListener {
            v: View ->
            dismiss()
        }
    }
}