package com.machut.pretmanagement

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class PopupAdd(private val adapter : MainActivity, private val recyclerAdapter: RecyclerAdapter) : Dialog(adapter) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.popup_add)

        setupAddButton()
        setupCloseButton()
    }

    private fun setupAddButton() {
        findViewById<ImageView>(R.id.icon_add).setOnClickListener {
                v: View ->
            val modifiedPret = PretModel(
                "",
                findViewById<EditText>(R.id.editTextEdit).text.toString(),
                findViewById<EditText>(R.id.editTextEdit2).text.toString(),
                findViewById<EditText>(R.id.editTextEdit3).text.toString(),
                findViewById<EditText>(R.id.editTextEdit4).text.toString().toFloat(),
                "",
                findViewById<EditText>(R.id.editTextEdit5).text.toString().toFloat()
            )
            PretRepository().add(modifiedPret, recyclerAdapter)
            dismiss()
        }
    }

    private fun setupCloseButton() {
        findViewById<ImageView>(R.id.icon_close).setOnClickListener {
                v: View ->
            dismiss()
        }
    }
}